import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { SitebarComponent } from './core/sidebar/sitebar.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CategoryDBService } from './core/services/categorydb.service';
import { TaskDBService } from './core/services/taskdb.service';
import { CategoryService } from './core/services/category.service';
import { TaskService } from './core/services/task.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SitebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    HttpClientModule
  ],
  providers: [
    TaskDBService,
    CategoryDBService,
    CategoryService,
    TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { TaskDBService } from 'src/app/core/services/taskdb.service';
import { TaskDo } from 'src/app/shared/models/TaskDo';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { CategoryDo } from 'src/app/shared/models/CategoryDo';
import { CategoryDBService } from 'src/app/core/services/categorydb.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  tasks: TaskDo[];
  currentCategory: CategoryDo;
  currentCategory$: Subscription;
  tasks$: Subscription;
  constructor(private taksService: TaskDBService, private categoryService: CategoryDBService) { }

  ngOnInit() {
    this.currentCategory$ = this.categoryService.current$
      .pipe(map(item => { this.currentCategory = item; this.filterByCategory(item); } ))
      .subscribe();

    this.tasks$ = this.taksService.todos$
      .subscribe((resp: TaskDo[]) => {
        this.tasks = resp;
        this.filterByCategory(this.currentCategory); });

  }

  filterByCategory(value?: CategoryDo) {
    let id = 'all';
    if (value) {
      id = value.id;
    }
    this.tasks = this.taksService.getTasksByCategoryId(id);
  }



  ngOnDestroy(): void {
    this.tasks$.unsubscribe();
    this.currentCategory$.unsubscribe();
  }
}

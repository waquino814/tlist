import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TaskDBService } from 'src/app/core/services/taskdb.service';
import { TaskDo } from 'src/app/shared/models/TaskDo';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryDo } from 'src/app/shared/models/CategoryDo';


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {

  @Input()
  task: TaskDo;
  @Output()
  recordUpdated = new EventEmitter<any>();
  showSuccessMsg = false;
  eddit = false;
  createForm: FormGroup;
  constructor(private taksService: TaskDBService, private fb: FormBuilder) { }


  ngOnInit() {
    this.createForm = this.fb.group({
      name: [''],
      description: ['', Validators.required],
      dueDate: [''],
      customCat: [false],
      personalCat: [false]
    });
  }

  addTask() {
    const t1 = new TaskDo();
    t1.name = this.createForm.value.name;
    t1.dueDate = this.createForm.value.dueDate;
    t1.description = this.createForm.value.description;
    t1.categories = new Array();
    if (this.createForm.value.customCat) {
      t1.categories = [...t1.categories, new CategoryDo('customCat') ];
    }
    if (this.createForm.value.personalCat) {
      t1.categories = [...t1.categories, new CategoryDo('personalCat') ];
    }
    this.taksService.addTask(t1);
    this.showSuccessMsg = true;
    this.clearForm();
  }
  edditTask() {
    // get info from the form
    // TODO
    // update the service
    this.taksService.updateTask(this.task);
  }
  clearForm() {
    this.createForm.patchValue({
      name: '',
      description: '',
      dueDate: '',
      customCat: false,
      personalCat: false
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTaskComponent } from './add-task.component';
import { AddTaskRoutingModule } from './add-task-routing.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AddTaskComponent],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AddTaskRoutingModule,
    ReactiveFormsModule
  ]
})
export class AddTaskModule { }

export class CategoryDo {
    id: string;
    name: string;
    description: string;
    icon: string;

    constructor(id?: string, name?: string) {
        this.id = id;
        this.name = name;
    }

}

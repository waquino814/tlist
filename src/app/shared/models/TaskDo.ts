import { CategoryDBService } from 'src/app/core/services/categorydb.service';
import { CategoryDo } from './CategoryDo';

export class TaskDo {
    id: string;
    name: string;
    dueDate: string;
    description: string;
    categories: CategoryDo[];
    started: boolean;
    completed: boolean;

}

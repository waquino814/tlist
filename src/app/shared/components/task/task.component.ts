import { Component, OnInit, Input } from '@angular/core';
import { TaskDo } from '../../models/TaskDo';
import { TaskDBService } from 'src/app/core/services/taskdb.service';
import { CategoryDo } from '../../models/CategoryDo';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  @Input()
  task: TaskDo;
  isStarted = false;
  isCompleted = false;
  constructor(private taskService: TaskDBService) { }

  ngOnInit() {
    if (this.task) {
      this.isStarted = this.task.started;
      this.isCompleted = this.task.completed;
    }
  }
  togleCompleted() {
    this.isCompleted = !this.isCompleted;
    this.task.completed = this.isCompleted;
    this.taskService.updateTask(this.task);
  }
  togleStarted() {
    this.isStarted = !this.isStarted;
    this.task.started = this.isStarted;
    this.taskService.updateTask(this.task);
  }

}

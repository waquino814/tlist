import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryDBService } from '../services/categorydb.service';
import { filter, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { CategoryDo } from 'src/app/shared/models/CategoryDo';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private currentCategory: CategoryDo;
  private showFilter = false;
  private id;
  private current$: Subscription;
  constructor(private router: Router, private route: ActivatedRoute, private categoryService: CategoryDBService) {

   }

  ngOnInit() {
    // in case i want to get the value from the url param
    this.route.queryParamMap.subscribe(queryParams => {
      this.id = queryParams.get('filter') ? queryParams.get('filter') : 'all';
      this.categoryService.setCurrentCategoryById(this.id);
      this.currentCategory = this.categoryService.current;
    });
    // in case i want to get the value from the observable
    /*this.current$ = this.categoryService.current$
    .pipe(map(item => (this.currentCategory = item)))
    .subscribe();*/
  }
  ngOnDestroy(): void {
    this.current$.unsubscribe();
  }
  toggleShowFilter() {
    this.showFilter = !this.showFilter;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TaskService {
  mockUrl = 'assets/mocks/tasks.json';
  constructor(private http: HttpClient) {
  }

  getMockTasks(): Observable<any> {
    return this.http.get(this.mockUrl);
  }
}

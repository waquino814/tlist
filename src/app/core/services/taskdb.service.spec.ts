import { TestBed } from '@angular/core/testing';

import { TaskDBService } from './taskdb.service';

describe('TaskDBService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaskDBService = TestBed.get(TaskDBService);
    expect(service).toBeTruthy();
  });
});

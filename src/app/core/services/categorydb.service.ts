import { Injectable } from '@angular/core';
import { CategoryDo } from 'src/app/shared/models/CategoryDo';
import { BehaviorSubject } from 'rxjs';
import { CategoryService } from './category.service';
import { take } from 'rxjs/operators';


@Injectable()
export class CategoryDBService {
  private readonly _categories = new BehaviorSubject<CategoryDo[]>([]);
  readonly categories$ = this._categories.asObservable();
  private readonly _current = new BehaviorSubject<CategoryDo>({
    name: 'Inbox',
    description: 'All your task goes here',
    id: 'all',
    icon: 'fa fa-inbox'
  });
  readonly current$ = this._current.asObservable();

  constructor(private categoryService: CategoryService) {
    this.categoryService.getMockCategories().pipe(take(1)).subscribe(
      {
        next : (resp) => this.categories = resp,
        error: (e) => console.log(e)
      });
  }

  set categories(val: CategoryDo[]) {
    this._categories.next(val);
  }

  get categories(): CategoryDo[] {
    return this._categories.getValue();
  }
  set current(val: CategoryDo) {
    this._current.next(val);
  }

  get current(): CategoryDo {
    return this._current.getValue();
  }

  setCurrentCategoryById(id: string) {
    const category = this._categories.getValue().find(item => item.id === id);
    this.current = category ? category : this.current;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoryDo } from 'src/app/shared/models/CategoryDo';

@Injectable()
export class CategoryService {
  private mockUrl = 'assets/mocks/categories.json';

  constructor(private http: HttpClient) { }
  public getMockCategories(): Observable<CategoryDo[]> {
    return this.http.get<CategoryDo[]>(this.mockUrl);
  }
}

import { Injectable } from '@angular/core';
import { TaskDo } from 'src/app/shared/models/TaskDo';
import { BehaviorSubject } from 'rxjs';
import { CategoryDo } from 'src/app/shared/models/CategoryDo';
import { TaskService } from './task.service';
import { take } from 'rxjs/operators';

@Injectable()
export class TaskDBService {
  private readonly _todos = new BehaviorSubject<TaskDo[]>([]);
  readonly todos$ = this._todos.asObservable();
  constructor(private taskService: TaskService) {
    this.refreshData();
  }

  get tasks(): TaskDo[] {
    return this._todos.getValue();
  }
  set tasks(val: TaskDo[]) {
    this._todos.next(val);
  }

  addTask(task: TaskDo) {
    this.tasks = [
      ...this.tasks, task
    ];
  }
  removeTask(id: string) {
    this.tasks = this._todos.getValue().filter(task => task.id !== id);
  }

  updateTask(task: TaskDo) {
    const todo = this.tasks.find( t => t.id === task.id);

    if (todo) {
      // we need to make a new copy of todos array, and the todo as well
      // remember, our state must always remain immutable
      // otherwise, on push change detection won't work, and won't update its view    

      const index = this.tasks.indexOf(todo);
      this.tasks[index] = task;
      this.tasks = [...this.tasks];
    }
  }

  public getTasksByCategoryId(category = 'all'): TaskDo[] {
    if (category === 'all') {
      return this._todos.getValue();
    } else if (category === 'completed') {
      return this._todos.getValue().filter(item => item.completed);
    } else if (category === 'started') {
      return this._todos.getValue().filter(item => item.started);
    } else if (this._todos.getValue() !=  null && typeof this._todos.getValue() !== 'undefined') {
      return this._todos.getValue().filter(item => {
        let found = false;
        if (item.categories !== null && typeof  item.categories !== 'undefined') {
        item.categories.forEach( c => {
           if (c.id === category) {found = true; }
          });
        }
        return found;
       });
    }
    return new Array();

  }

  private refreshData() {
    this.taskService.getMockTasks().pipe(take(1)).subscribe({
      next: resp => this._todos.next(resp),
      error: e => console.log(e)
    });
  }
}

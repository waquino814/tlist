import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryDBService } from '../services/categorydb.service';

@Component({
  selector: 'app-sitebar',
  templateUrl: './sitebar.component.html',
  styleUrls: ['./sitebar.component.scss']
})
export class SitebarComponent implements OnInit {
  @Output() siteItemClick = new EventEmitter<string>();

  constructor(private categoryService: CategoryDBService) { }

  ngOnInit() {
  }
  filterByCategory(value: string) {
    this.categoryService.setCurrentCategoryById(value);
  }
}
